---
layout: markdown_page
title: "Partner marketing"
---

Welcome to the Partner Marketing Handbook

[Up one level to the Product Marketing Handbook](/handbook/marketing/product-marketing/)    

## On this page
* [Partner Marketing](#partnermarketing)

## Partner Marketing Objectives<a name="partnermarketing"></a>

- Promote existing partnerships to be at top-of-mind for developers
- Integrate resale partnerships: promote partnership integrations/products which we sell as part of our sales process.
- Migrate open source projects to adopt GitLab, and convert their users in-turn to GitLab.
- Surface the ease of GitLab integration to encourage more companies to integrate with GitLab.
- Build closer relationships with existing partners through consistent communication

- [Strategic Partner listing](https://docs.google.com/document/d/1-oAf0tMlTrAaPAsG_8NLXrI3DEZqI5ZA0gW0lKxFjA4/edit) (internal)
